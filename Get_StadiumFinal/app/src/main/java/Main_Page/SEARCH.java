package Main_Page;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import la.get_stadiumfinal.R;

public class SEARCH extends AppCompatActivity {

    /**---Main Button---**/
    Button Join_Button,Search_Button,MyMatch_Button,Setting_Button;

    /**---search setting ---**/
    private ListView lstSearch;
    private EditText edtSearch;
    private ArrayAdapter<String> adapter;

    String data[] = {
            "F Stadium","P Stadium","K Stadium","D Stadium","Pr Stadium"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        /**---ID Mapping---**/
        /**---Main Button---**/
        Join_Button = (Button)findViewById(R.id.JoinB);
        Search_Button = (Button)findViewById(R.id.SearchB);
        MyMatch_Button =  (Button)findViewById(R.id.MatchB);
        Setting_Button = (Button)findViewById(R.id.SetingB);

        /**---JOIN ONCLICK---**/
        Join_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent JoinIntent = new Intent(SEARCH.this, JOIN.class);
                startActivity(JoinIntent);
                finish();

            }
        });
        /**---SEARCH ONCLICK---**/
        Search_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent SearchIntent = new Intent(SEARCH.this, SEARCH.class);
                startActivity(SearchIntent);
                finish();

            }
        });
        /**---MATCH ONCLICK---**/
        MyMatch_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent MatchIntent = new Intent(SEARCH.this, MATCH.class);
                startActivity(MatchIntent);
                finish();

            }
        });
        /**---SETTING ONCLICK---**/
        Setting_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent SettingIntent = new Intent(SEARCH.this, SETTING.class);
                startActivity(SettingIntent);
                finish();

            }
        });

        /**---SEARCH SET---**/
        lstSearch = (ListView)findViewById(R.id.lstSearch);
        edtSearch = (EditText)findViewById(R.id.edtSearch);
        adapter = new ArrayAdapter<String>(this,R.layout.list_item,R.id.textView,data);
        lstSearch.setAdapter(adapter);

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                SEARCH.this.adapter.getFilter().filter(charSequence);

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

}
