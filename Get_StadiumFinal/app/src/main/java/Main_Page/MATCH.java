package Main_Page;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import la.get_stadiumfinal.R;

public class MATCH extends AppCompatActivity {

    /**---Main Button---**/
    Button Join_Button,Search_Button,MyMatch_Button,Setting_Button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match);

        /**---ID Mapping---**/
        /**---Main Button---**/
        Join_Button = (Button)findViewById(R.id.JoinB);
        Search_Button = (Button)findViewById(R.id.SearchB);
        MyMatch_Button =  (Button)findViewById(R.id.MatchB);
        Setting_Button = (Button)findViewById(R.id.SetingB);

        /**---JOIN ONCLICK---**/
        Join_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent JoinIntent = new Intent(MATCH.this, JOIN.class);
                startActivity(JoinIntent);
                finish();

            }
        });
        /**---SEARCH ONCLICK---**/
        Search_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent SearchIntent = new Intent(MATCH.this, SEARCH.class);
                startActivity(SearchIntent);
                finish();

            }
        });
        /**---MATCH ONCLICK---**/
        MyMatch_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent MatchIntent = new Intent(MATCH.this, MATCH.class);
                startActivity(MatchIntent);
                finish();

            }
        });
        /**---SETTING ONCLICK---**/
        Setting_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent SettingIntent = new Intent(MATCH.this, SETTING.class);
                startActivity(SettingIntent);
                finish();

            }
        });
    }
}
