package Main_Page;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import Sub_Page.RESERVE_DETAIL;
import la.get_stadiumfinal.MainActivity;
import la.get_stadiumfinal.R;

public class HOME extends AppCompatActivity {

    /**---Main Button---**/
    Button Join_Button,Search_Button,MyMatch_Button,Setting_Button,testbut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_);



        /**---ID Mapping---**/
            /**---Main Button---**/
            Join_Button = (Button)findViewById(R.id.JoinB);
            Search_Button = (Button)findViewById(R.id.SearchB);
            MyMatch_Button =  (Button)findViewById(R.id.MatchB);
            Setting_Button = (Button)findViewById(R.id.SetingB);
            testbut = (Button) findViewById(R.id.test_btn);
        /**---JOIN ONCLICK---**/
        Join_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent JoinIntent = new Intent(HOME.this, JOIN.class);
                startActivity(JoinIntent);

            }
        });
        /**---SEARCH ONCLICK---**/
        Search_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent SearchIntent = new Intent(HOME.this, SEARCH.class);
                startActivity(SearchIntent);

            }
        });
        /**---MATCH ONCLICK---**/
        MyMatch_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent MatchIntent = new Intent(HOME.this, MATCH.class);
                startActivity(MatchIntent);

            }
        });
        /**---SETTING ONCLICK---**/
        Setting_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent SettingIntent = new Intent(HOME.this, SETTING.class);
                startActivity(SettingIntent);

            }
        });

        testbut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ReserveIntent = new Intent(HOME.this, RESERVE_DETAIL.class);
                startActivity(ReserveIntent);
            }
        });

    }
}
