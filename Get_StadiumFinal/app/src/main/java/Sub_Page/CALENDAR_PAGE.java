package Sub_Page;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.CalendarView;

import la.get_stadiumfinal.R;

/**
 * Created by Folk-Chan on 6/2/2561.
 */

public class CALENDAR_PAGE extends AppCompatActivity {

    private static final String TAG = "CALENDAR_PAGE";
    private CalendarView calenview;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calendar_layout);
        calenview = (CalendarView) findViewById(R.id.calendarView);

        calenview.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int y, int m, int d) {
                String date = d + "/" + (m+1) + "/" + y;
                Log.d(TAG, "onSelectedDayChange: dd/mm/yyyy: " + date);

                Intent calenintent = new Intent(CALENDAR_PAGE.this,RESERVE_TIME.class);
                calenintent.putExtra("date",date);
                startActivity(calenintent);
            }
        });
    }
}
