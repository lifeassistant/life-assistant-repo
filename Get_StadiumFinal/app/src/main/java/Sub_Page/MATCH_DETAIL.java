package Sub_Page;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import Main_Page.JOIN;
import Main_Page.MATCH;
import Main_Page.SEARCH;
import Main_Page.SETTING;
import la.get_stadiumfinal.R;

public class MATCH_DETAIL extends AppCompatActivity {

    int i=0,p=0;
    TextView showply;
    String ply;
    Button Join_Button,Search_Button,MyMatch_Button,Setting_Button,exitbut;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef,myRef2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match__detail);

        //myRef = FirebaseDatabase.getInstance().getReference("User");
        myRef2 = FirebaseDatabase.getInstance().getReference("Player");
        showply = (TextView)findViewById(R.id.show_player);
        Join_Button = (Button)findViewById(R.id.JoinB);
        Search_Button = (Button)findViewById(R.id.SearchB);
        MyMatch_Button =  (Button)findViewById(R.id.MatchB);
        Setting_Button = (Button)findViewById(R.id.SetingB);
        exitbut = (Button) findViewById(R.id.exit_btn);





        myRef2.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ply = dataSnapshot.getValue().toString();
                //showply.setText("Player: " + ply + " / 11");
                p = Integer.parseInt(ply);
                showply.setText("Player: " + String.valueOf(p+i) + " / 11");
                i++;
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        exitbut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent MyMatchIntent = new Intent(MATCH_DETAIL.this,MATCH.class);
                startActivity(MyMatchIntent);
                p = Integer.parseInt(ply);
                myRef2 = database.getReference("Player");
                myRef2.setValue(p-1);
                i--;

            }
        });





        /**---JOIN ONCLICK---**/
        Join_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent JoinIntent = new Intent(MATCH_DETAIL.this, JOIN.class);
                startActivity(JoinIntent);


            }
        });
        /**---SEARCH ONCLICK---**/
        Search_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent SearchIntent = new Intent(MATCH_DETAIL.this, SEARCH.class);
                startActivity(SearchIntent);


            }
        });
        /**---MATCH ONCLICK---**/
        MyMatch_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent MatchIntent = new Intent(MATCH_DETAIL.this, MATCH.class);
                startActivity(MatchIntent);


            }
        });
        /**---SETTING ONCLICK---**/
        Setting_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent SettingIntent = new Intent(MATCH_DETAIL.this, SETTING.class);
                startActivity(SettingIntent);


            }
        });


    }
}
