package Sub_Page;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import la.get_stadiumfinal.R;

public class RESERVE_DETAIL extends AppCompatActivity {

    Button res_btn;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reserve__detail);


        res_btn = (Button) findViewById(R.id.reserve_btn);


        res_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ResTimeIntent = new Intent(RESERVE_DETAIL.this,RESERVE_TIME.class);
                startActivity(ResTimeIntent);

            }
        });
    }

}
