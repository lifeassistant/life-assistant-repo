package Sub_Page;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import Main_Page.JOIN;
import Main_Page.MATCH;
import Main_Page.SEARCH;
import Main_Page.SETTING;
import la.get_stadiumfinal.R;

public class RESERVE_TIME extends AppCompatActivity {

    int p=1;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef2;
    Button confirmbut,Join_Button,Search_Button,MyMatch_Button,Setting_Button,calendarbut;
    TextView showdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reserve__time);


        myRef2 = FirebaseDatabase.getInstance().getReference("Player");

        confirmbut = (Button)findViewById(R.id.conF);
        Join_Button = (Button)findViewById(R.id.JoinB);
        Search_Button = (Button)findViewById(R.id.SearchB);
        MyMatch_Button =  (Button)findViewById(R.id.MatchB);
        Setting_Button = (Button)findViewById(R.id.SetingB);
        calendarbut = (Button) findViewById(R.id.calendar_btn);
        showdate = (TextView) findViewById(R.id.date_txt);


        calendarbut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent CalendarIntent = new Intent(RESERVE_TIME.this,CALENDAR_PAGE.class);
                startActivity(CalendarIntent);
            }
        });

        Intent incomintent = getIntent();
        String date = incomintent.getStringExtra("date");
        showdate.setText(date);

        /**---JOIN ONCLICK---**/
        Join_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent JoinIntent = new Intent(RESERVE_TIME.this, JOIN.class);
                startActivity(JoinIntent);


            }
        });
        /**---SEARCH ONCLICK---**/
        Search_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent SearchIntent = new Intent(RESERVE_TIME.this, SEARCH.class);
                startActivity(SearchIntent);


            }
        });
        /**---MATCH ONCLICK---**/
        MyMatch_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent MatchIntent = new Intent(RESERVE_TIME.this, MATCH.class);
                startActivity(MatchIntent);


            }
        });
        /**---SETTING ONCLICK---**/
        Setting_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent SettingIntent = new Intent(RESERVE_TIME.this, SETTING.class);
                startActivity(SettingIntent);


            }
        });

        confirmbut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent KickIntent = new Intent(RESERVE_TIME.this, MATCH_DETAIL.class);
                startActivity(KickIntent);
                myRef2 = database.getReference("Player");
                myRef2.setValue(p);


            }
        });




    }
}
