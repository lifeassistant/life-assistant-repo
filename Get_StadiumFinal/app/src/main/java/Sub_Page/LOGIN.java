package Sub_Page;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import la.get_stadiumfinal.R;

public class LOGIN extends AppCompatActivity {

    TextView textView;
    Typeface typeface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);



        textView = (TextView)findViewById(R.id.textView);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Sweet Sensations Personal Use.ttf");
        textView.setTypeface(typeface);

    }
}
