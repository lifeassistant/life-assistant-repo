package la.get_stadiumfinal;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ExpandableListView;

import Main_Page.HOME;

public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        Intent HomeIntent = new Intent(MainActivity.this, HOME.class);
        startActivity(HomeIntent);
        finish();
    }
}
